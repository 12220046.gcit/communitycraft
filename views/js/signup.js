import { showAlert } from './alert.js'

export const signup = async (name, email, password, passwordConfirm, phoneno, address, user) => {
    try{
        const res = await axios ({
            method: 'POST',
            url : 'http://localhost:4001/api/v1/users/signup',
            data:{
                name,
                email,
                password,
                passwordConfirm,
                phoneno,
                address,
                user,
            },    
        }) 
        if (res.data.status === 'success'){
            showAlert('success', 'Account created successfully!')
            window.setTimeout(() => {
                location.assign('/homepage.html')
            }, 1000)
        }
    } catch(err){
        let message = 
        typeof err.response !== 'undefined'
        ? err.response.data.message
        : err.message
        showAlert('error', 'Error: Passwords are not the same! ', message)
    }
}

document.querySelector('.form').addEventListener('submit', (e) =>{
    e.preventDefault()
    const name = document.getElementById('name').value
    const email = document.getElementById('email').value
    const password = document.getElementById('password').value
    const passwordConfirm = document.getElementById('confirm_password').value
    var phoneno = document.getElementById('phoneno').value
    var address = document.getElementById('address').value
    var user = document.querySelector('input[name=user]:checked').value;

    let  isNameValid = checkName(),
         isEmailValid = checkEmail(),
         isPasswordValid = checkPassword(),
         isConfirmPasswordValid = checkConfrimPassword(),
         isPhoneNoValid = checkPhoneNo(),
         isAddressValid = checkAddress();
    let isFormValid =  isNameValid && isEmailValid  && isPasswordValid && isConfirmPasswordValid && isPhoneNoValid && isAddressValid;

     if (isFormValid){
        // window.location.href = "index.html"
        signup(name,email,password, passwordConfirm, phoneno, address, user)
     }
})
var nameInput = document.getElementById('name')
var emailInput = document.getElementById('email')
var passwordInput = document.getElementById('password')
var confirmPasswordInput = document.getElementById('confirm_password')
var phonenoInput = document.getElementById('phoneno')
var addressInput = document.getElementById('address')


// const validateForm = (event) => {
//     event.preventDefault();
//     let  isNameValid = checkName(),
//          isEmailValid = checkEmail(),
//          isPasswordValid = checkPassword(),
//          isConfirmPasswordValid = checkConfrimPassword(),
//          isPhoneNoValid = checkPhoneNo(),
//          isAddressValid = checkAddress();
//      let isFormValid =  isNameValid && isEmailValid  && isPasswordValid && isConfirmPasswordValid && isPhoneNoValid && isAddressValid;

//      if (isFormValid){
//         // window.location.href = "index.html"

//      }
// }
    
var nameError = document.getElementById('nameError');
var emailError = document.getElementById('emailError');
var passwordError = document.getElementById('passwordError');
var confirmPasswordError = document.getElementById('confirm_passwordError');
var phoneNoError = document.getElementById("phonenoError");
var addressError = document.getElementById('addressError');

nameInput.addEventListener('input', function(){
    checkName()
})
emailInput.addEventListener('input', function(){
    checkEmail()
})
passwordInput.addEventListener('input',function(){
    checkPassword();
})
confirmPasswordInput.addEventListener('input', function(){
    checkConfrimPassword();
})
phonenoInput.addEventListener('input', function(){
    checkPhoneNo()
})
addressInput.addEventListener('input', function(){
    checkAddress()
})
const checkName = () => {
    if (!isRequired(nameInput.value)){
        nameInput.classList.add('is-invalid');
        nameInput.classList.remove('is-valid');
        nameError.innerHTML = 'Name cannot be blank';
        return false;
    } 
    else if(!isNameValid(nameInput.value)){
        nameInput.classList.add('is-invalid');
        nameInput.classList.remove('is-valid');
        nameError.innerHTML = 'Name should be between 3 and 50 characters';
        return false;
    }
     else {
        nameInput.classList.add('is-valid');
        nameInput.classList.remove('is-invalid');
        nameError.innerHTML = '';
        return true;
    }
}
const isNameValid = () => {
    if (nameInput.value.length >= 3 && nameInput.value.length <= 50) {
        return true;
    }
    return false;
}
const checkEmail = () => {
    if (!isRequired(emailInput.value)){
        emailInput.classList.add('is-invalid');
        emailInput.classList.remove('is-valid');
        emailError.innerHTML = 'Email cannot be blank.';
        return false;
    } else if (!isEmailValid(emailInput.value)){
        emailInput.classList.add('is-invalid');
        emailInput.classList.remove('is-valid');
        emailError.innerHTML = 'Email is not valid.';
        return false;
    } else{
        emailInput.classList.add('is-valid');
        emailInput.classList.remove('is-invalid');
        emailError.innerHTML = '';
        return true;
    }
}
const isEmailValid = (email) => {
    const re = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;
    return re.test(email);
};

const isRequired = value => value.trim() === ''? false : true;
// const isRequired = value => !value;

const checkPassword = () => {
    if (!isRequired(passwordInput.value)){
        passwordInput.classList.add('is-invalid');
        passwordInput.classList.remove('is-valid');
        passwordError.innerHTML = 'Password cannot be blank.';
        return false;
    } else if(!isPasswordValid(passwordInput.value)){
        passwordInput.classList.add('is-invalid');
        passwordInput.classList.remove('is-valid');
        passwordError.innerHTML = 'Password must has at least 8 characters that include atleast 1 lowercase character, 1 uppercase characters, 1 number, and 1 special character';
        return false;
    } else {
        passwordInput.classList.add('is-valid');
        passwordInput.classList.remove('is-invalid');
        passwordError.innerHTML = '';
        return true;
    }
}
const isPasswordValid = (password) => {
    const re = new
    RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");
    return re.test(password);
}

const checkConfrimPassword = () => {
    if (!isRequired(confirmPasswordInput.value)){
        confirmPasswordInput.classList.add('is-invalid');
        confirmPasswordInput.classList.remove('is-valid');
        confirmPasswordError.innerHTML = 'Please enter the password again.';
        return false;
    } else if (confirmPasswordInput.value !== passwordInput.value){
        confirmPasswordInput.classList.add('is-invalid');
        confirmPasswordInput.classList.remove('is-valid');
        confirmPasswordError.innerHTML = 'The password does not match.';
        return false;
    } else {
        confirmPasswordInput.classList.add('is-valid');
        confirmPasswordInput.classList.remove('is-invalid');
        confirmPasswordError.innerHTML = '';
        return true;
    }
}
const checkPhoneNo = () => {
    if (!isRequired(phonenoInput.value)) {
      phonenoInput.classList.add('is-invalid');
      phonenoInput.classList.remove('is-valid');
      phoneNoError.innerHTML = 'Phone number cannot be blank.';
      return false;
    } else if (!isPhoneNoValid(phonenoInput.value)) {
      phonenoInput.classList.add('is-invalid');
      phonenoInput.classList.remove('is-valid');
      phoneNoError.innerHTML = 'Phone number is not valid. Please use the format 77/17 XXXXXXX';
      return false;
    } else {
      phonenoInput.classList.add('is-valid');
      phonenoInput.classList.remove('is-invalid');
      phoneNoError.innerHTML = '';
      return true;
    }
  };
const isPhoneNoValid = (phoneno) => {
    const re = /^(77|17)\d{6}$/;
    return re.test(phoneno);
}
const checkAddress = () => {
    if (!isRequired(addressInput.value)){
        addressInput.classList.add('is-invalid');
        addressInput.classList.remove('is-valid');
        addressError.innerHTML = 'Address cannot be blank';
        return false;
    } 
    else if(!isAddressValid(addressInput.value)){
        addressInput.classList.add('is-invalid');
        addressInput.classList.remove('is-valid');
        addressError.innerHTML = 'Address should be between 3 and 50 characters';
        return false;
    }
     else {
        addressInput.classList.add('is-valid');
        addressInput.classList.remove('is-invalid');
        addressError.innerHTML = '';
        return true;
    }
}
const isAddressValid = () => {
    if (addressInput.value.length >= 3 && addressInput.value.length <= 50) {
        return true;
    }
    return false;
}