document.getElementById('close-icon').addEventListener('click', function() {
    window.location.href = 'index.html'; // Replace 'home.html' with the actual URL of your home page
});

  
var nameInput = document.getElementById('name')
var emailInput = document.getElementById('email')
var phonenoInput = document.getElementById('phoneno')
var addressInput = document.getElementById('address')
var quantityInput = document.getElementById('quantity')
var descriptionInput = document.getElementById('description')

// const validateForm = (event) => {
//     event.preventDefault();

//     let  isNameValid = checkName(),
//          isEmailValid = checkEmail(),
//          isPhoneNoValid = checkPhoneNo(),
//          isAddressValid = checkAddress(),
//          isQuantityValid = checkQuantity(),
//          isDescriptionValid = checkDescription();
//      let isFormValid =  isNameValid && isEmailValid && isPhoneNoValid && isAddressValid && isQuantityValid && isDescriptionValid;

//      if (isFormValid){
//         window.location.href = 'home.html'
//      } 
// }
document.getElementById('request-form').addEventListener('submit', function(event) {
    event.preventDefault(); // Prevent the form from submitting by default

    let isNameValid = checkName();
    let isEmailValid = checkEmail();
    let isPhoneNoValid = checkPhoneNo();
    let isAddressValid = checkAddress();
    let isQuantityValid = checkQuantity();
    let isDescriptionValid = checkDescription();

    let isFormValid = isNameValid && isEmailValid && isPhoneNoValid && isAddressValid && isQuantityValid && isDescriptionValid;

    if (isFormValid) {
        window.location.href = 'index.html'; // Navigate to the home page on successful form submission
    }
});  
var nameError = document.getElementById('nameError');
var emailError = document.getElementById('emailError');
var phoneNoError = document.getElementById("phonenoError");
var addressError = document.getElementById('addressError');
var quantityError = document.getElementById('quantityError');
var descriptionError = document.getElementById('descriptionError');

nameInput.addEventListener('input', function(){
    checkName()
})
emailInput.addEventListener('input', function(){
    checkEmail()
})
phonenoInput.addEventListener('input', function(){
    checkPhoneNo()
})
addressInput.addEventListener('input', function(){
    checkAddress()
})
quantityInput.addEventListener('input',function(){
    checkQuantity();
})
descriptionInput.addEventListener('input', function(){
    checkDescription();
})
const checkName = () => {
    if (!isRequired(nameInput.value)){
        nameInput.classList.add('is-invalid');
        nameInput.classList.remove('is-valid');
        nameError.innerHTML = 'Name cannot be blank';
        return false;
    } 
    else if(!isNameValid(nameInput.value)){
        nameInput.classList.add('is-invalid');
        nameInput.classList.remove('is-valid');
        nameError.innerHTML = 'Name should be between 3 and 50 characters';
        return false;
    }
     else {
        nameInput.classList.add('is-valid');
        nameInput.classList.remove('is-invalid');
        nameError.innerHTML = '';
        return true;
    }
}
const isNameValid = () => {
    if (nameInput.value.length >= 3 && nameInput.value.length <= 50) {
        return true;
    }
    return false;
}
const checkEmail = () => {
    if (!isRequired(emailInput.value)){
        emailInput.classList.add('is-invalid');
        emailInput.classList.remove('is-valid');
        emailError.innerHTML = 'Email cannot be blank.';
        return false;
    } else if (!isEmailValid(emailInput.value)){
        emailInput.classList.add('is-invalid');
        emailInput.classList.remove('is-valid');
        emailError.innerHTML = 'Email is not valid.';
        return false;
    } else{
        emailInput.classList.add('is-valid');
        emailInput.classList.remove('is-invalid');
        emailError.innerHTML = '';
        return true;
    }
}
const isEmailValid = (email) => {
    const re = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;
    return re.test(email);
};

const isRequired = value => value.trim() === ''? false : true;
// const isRequired = value => !value;

const checkPhoneNo = () => {
    if (!isRequired(phonenoInput.value)) {
      phonenoInput.classList.add('is-invalid');
      phonenoInput.classList.remove('is-valid');
      phoneNoError.innerHTML = 'Phone number cannot be blank.';
      return false;
    } else if (!isPhoneNoValid(phonenoInput.value)) {
      phonenoInput.classList.add('is-invalid');
      phonenoInput.classList.remove('is-valid');
      phoneNoError.innerHTML = 'Phone number is not valid. Please use the format 77/17 XXXXXXX';
      return false;
    } else {
      phonenoInput.classList.add('is-valid');
      phonenoInput.classList.remove('is-invalid');
      phoneNoError.innerHTML = '';
      return true;
    }
  };
const isPhoneNoValid = (phoneno) => {
    const re = /^(77|17)\d{6}$/;
    return re.test(phoneno);
}
const checkAddress = () => {
    if (!isRequired(addressInput.value)){
        addressInput.classList.add('is-invalid');
        addressInput.classList.remove('is-valid');
        addressError.innerHTML = 'Address cannot be blank';
        return false;
    } 
    else if(!isAddressValid(addressInput.value)){
        addressInput.classList.add('is-invalid');
        addressInput.classList.remove('is-valid');
        addressError.innerHTML = 'Address should be between 3 and 50 characters';
        return false;
    }
     else {
        addressInput.classList.add('is-valid');
        addressInput.classList.remove('is-invalid');
        addressError.innerHTML = '';
        return true;
    }
}
const isAddressValid = () => {
    if (addressInput.value.length >= 3 && addressInput.value.length <= 50) {
        return true;
    }
    return false;
}
const checkQuantity = () => {
    if (!isRequired(quantityInput.value)){
        quantityInput.classList.add('is-invalid');
        quantityInput.classList.remove('is-valid');
        quantityError.innerHTML = 'Quantity cannot be blank.';
        return false;
    } 
    else if (!isQuantityValid(quantityInput.value)){
        quantityInput.classList.add('is-invalid');
        quantityInput.classList.remove('is-valid');
        quantityError.innerHTML = 'Number of quantiy should be above 3';
        return false;
    } 
    else{
        quantityInput.classList.add('is-valid');
        quantityInput.classList.remove('is-invalid');
        quantityError.innerHTML = '';
        return true;
    }
}

const isQuantityValid = () => {
    if (quantityInput.value >= 3 && quantityInput.value <= 300) {
        return true;
    }
    return false;
}
const checkDescription = () => {
    if (!isRequired(descriptionInput.value)){
        descriptionInput.classList.add('is-invalid');
        descriptionInput.classList.remove('is-valid');
        descriptionError.innerHTML = 'Description cannot be blank';
        return false;
    } 
    else if(!isDescriptionValid(descriptionInput.value)){
        descriptionInput.classList.add('is-invalid');
        descriptionInput.classList.remove('is-valid');
        descriptionError.innerHTML = 'Description should be between 3 and 300 characters';
        return false;
    }
     else {
        descriptionInput.classList.add('is-valid');
        descriptionInput.classList.remove('is-invalid');
        descriptionError.innerHTML = '';
        return true;
    }
}
const isDescriptionValid = () => {
    if (descriptionInput.value.length >= 3 && descriptionInput.value.length <= 300) {
        return true;
    }
    return false;
}