import { showAlert } from "./alert.js";

const login = async (email, password) => {
    try{
        const res = await axios({
            method:'POST',
            url :'http://localhost:4001/api/v1/users/login',
            data:{
                email,
                password,
            },
        })
        if (res.data.status === 'success'){
            showAlert('success','Logged in successfully')
            window.setTimeout(() => {
                location.assign('/homepage.html')
            }, 1000)
        }
    } catch(err){
        let message = 
            typeof err.response !== 'undefined'
                ?err.response.data.message
                :err.message
        showAlert('error','Error : Incorrect email or password', message)
    }
}

var emailInput = document.getElementById('email');
var passwordInput = document.getElementById('password');

document.querySelector('.form').addEventListener('submit', (e) => {
    e.preventDefault();
    const email = document.getElementById('email').value;
    const password = document.getElementById('password').value;
    // Run the client-side validation checks
    let isEmailValid = checkEmail(),
        isPasswordValid = checkPassword();
    let isFormValid = isEmailValid && isPasswordValid;

    if (isFormValid){
        login(email, password);
    }
});
// const validateForm = (event) => {
//     event.preventDefault();
//     let isEmailValid = checkEmail(),
//         isPasswordValid = checkPassword();
//     let isFormValid = isEmailValid && isPasswordValid;

//     if (isFormValid){
//         login(email, password);
//     }
// }
         
var emailError = document.getElementById('emailError');
var passwordError = document.getElementById('passwordError');

emailInput.addEventListener('input', function(){
    checkEmail()
})
passwordInput.addEventListener('input',function(){
    checkPassword();
})

const checkEmail = () => {
    if (!isRequired(emailInput.value)){
        emailInput.classList.add('is-invalid');
        emailInput.classList.remove('is-valid');
        emailError.innerHTML = 'Email cannot be blank.';
        return false;
    } else if (!isEmailValid(emailInput.value)){
        emailInput.classList.add('is-invalid');
        emailInput.classList.remove('is-valid');
        emailError.innerHTML = 'Email is not valid.';
        return false;
    } else{
        emailInput.classList.add('is-valid');
        emailInput.classList.remove('is-invalid');
        emailError.innerHTML = '';
        return true;
    }
}
const isEmailValid = (email) => {
    const re = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;
    return re.test(email);
};

const isRequired = value => value.trim() === ''? false : true;

const checkPassword = () => {
    if (!isRequired(passwordInput.value)){
        passwordInput.classList.add('is-invalid');
        passwordInput.classList.remove('is-valid');
        passwordError.innerHTML = 'Password cannot be blank.';
        return false;
    } else if(!isPasswordValid(passwordInput.value)){
        passwordInput.classList.add('is-invalid');
        passwordInput.classList.remove('is-valid');
        passwordError.innerHTML = 'Password must has at least 8 characters that include atleast 1 lowercase character, 1 uppercase characters, 1 number, and 1 special character';
        return false;
    } else {
        passwordInput.classList.add('is-valid');
        passwordInput.classList.remove('is-invalid');
        passwordError.innerHTML = '';
        return true;
    }
}
const isPasswordValid = (password) => {
    const re = new
    RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");
    return re.test(password);
}