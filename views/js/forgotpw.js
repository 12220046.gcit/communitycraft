var emailInput = document.getElementById('email');

    const validateForm = (event) => {
        event.preventDefault();
        let isEmailValid = checkEmail();
        let isFormValid = isEmailValid;

        if (isFormValid) {
            window.location.href = "forgotverify.html";
        }
    }


    var emailInput = document.getElementById('email');
    var emailError = document.getElementById('emailError');

    emailInput.addEventListener('input', function(){
        checkEmail()
    })

    const checkEmail = () => {
        const email = emailInput.value.trim()
        if (!isRequired(emailInput.value)){
            emailInput.classList.add('is-invalid');
            emailInput.classList.remove('is-valid');
            emailError.innerHTML = 'Email cannot be blank.';
            return false;
        } else if (!isEmailValid(email)){
            emailInput.classList.add('is-invalid');
            emailInput.classList.remove('is-valid');
            emailError.innerHTML = 'Email is not valid.';
            return false;
        } else{
            emailInput.classList.add('is-valid');
            emailInput.classList.remove('is-invalid');
            emailError.innerHTML = '';
            return true;
        }
    }
    
    const isRequired = value => value.trim() === ''? false : true;
    const isEmailValid = (email) => {
        const re = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;
        return re.test(email);
      };
      
