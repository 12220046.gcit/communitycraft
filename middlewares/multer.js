const multer = require("multer");

// Define the storage configuration for image uploads
const imageStorage = multer.diskStorage({});

// Create a file filter function for images
const imageFileFilter = (req, file, cb) => {
  if (!file.mimetype.startsWith("image")) {
    cb("Supported only image files!", false);
  } else {
    cb(null, true);
  }
};

// Initialize Multer for image uploads
exports.uploadImage = multer({
  storage: imageStorage,
  fileFilter: imageFileFilter,
});
