const cloudinary = require("../cloud");

exports.sendError = (res, error, statusCode = 401) =>
  res.status(statusCode).json({ error });

exports.uploadImageToCloud = async (file) => {
  const { secure_url: url, public_id } = await cloudinary.uploader.upload(
    file,
    { gravity: "face", height: 500, width: 500, crop: "thumb" }
  );

  return { url, public_id };
};

exports.formatProduct = (product) => {
  const { name, category, nutrNum, quantity, description, _id, image } =
    product;
  return {
    id: _id,
    name,
    category,
    nutrNum,
    quantity,
    description,
    image: image?.url,
  };
};
