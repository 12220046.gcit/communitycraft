const express = require("express");
// const session = require('express-session');
const path = require("path");
const cors = require("cors");
const app = express();

app.use(cors());
app.use(express.json());
const userRouter = require("./routes/userRoutes");
const viewRouter = require("./routes/viewRoutes");
const productRouter = require("./routes/productRoutes");
app.use("/api/v1/users", userRouter);
app.use("/", viewRouter);
app.use("/api/products", productRouter);

app.use(express.static(path.join(__dirname, "views")));
module.exports = app;
