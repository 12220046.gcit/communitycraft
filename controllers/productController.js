// const Product = require('./../models/productModels');
// const { Storage } = require('@google-cloud/storage'); // Import Google Cloud Storage library

// const storage = new Storage();
// const bucketName = 'localSupplier'; // Replace with your actual bucket name

// // Get all products
// exports.getAllProducts = async (req, res, next) => {
//   try {
//     const products = await Product.find();
//     res.status(200).json({ data: products, status: 'success' });
//   } catch (err) {
//     res.status(500).json({ error: err.message });
//   }
// };

// // Create a new product with image upload
// exports.createProduct = async (req, res) => {
//   try {
//     const productData = req.body;

//     // Upload the image to Google Cloud Storage
//     const imageFile = req.file; // Assuming you use middleware to handle file uploads
//     if (imageFile) {
//       const bucket = storage.bucket(bucketName);
//       const imageUploadStream = bucket.file(imageFile.originalname).createWriteStream({
//         metadata: {
//           contentType: imageFile.mimetype,
//         },
//       });

//       imageUploadStream.on('error', (err) => {
//         console.error(err);
//         return res.status(500).json({ error: 'Image upload failed' });
//       });

//       imageUploadStream.on('finish', async () => {
//         productData.imageURL = `https://storage.googleapis.com/${bucketName}/${imageFile.originalname}`;

//         // Create the product with the image URL
//         const product = await Product.create(productData);
//         res.json({ data: product, status: 'success' });
//       });

//       req.file.stream.pipe(imageUploadStream);
//     } else {
//       // Handle cases where no image was uploaded
//       const product = await Product.create(productData);
//       res.json({ data: product, status: 'success' });
//     }
//   } catch (err) {
//     res.status(500).json({ error: err.message });
//   }
// };

// // Get a single product by ID
// exports.getProduct = async (req, res) => {
//   try {
//     const product = await Product.findById(req.params.id);
//     res.json({ data: product, status: 'success' });
//   } catch (err) {
//     res.status(500).json({ error: err.message });
//   }
// };

// // Update a product by ID
// exports.updateProduct = async (req, res) => {
//   try {
//     const product = await Product.findByIdAndUpdate(req.params.id, req.body);
//     res.json({ data: product, status: 'success' });
//   } catch (err) {
//     res.status(500).json({ error: err.message });
//   }
// };

// // Delete a product by ID
// exports.deleteProduct = async (req, res) => {
//   try {
//     const product = await Product.findByIdAndDelete(req.params.id);
//     res.json({ data: product, status: 'success' });
//   } catch (err) {
//     res.status(500).json({ error: err.message });
//   }
// };

// const Product = require("../models/product"); // Import your product model
// const { isValidObjectId } = require("mongoose");
// const { sendError } = require("../utils/helper");
// const cloudinary = require("../cloud"); // Import your cloudinary setup

// exports.createProduct = async (req, res) => {
//   try {
//     const { file, body } = req;

//     const { name, category, nutrNum, quantity, description } = body;

//     const newProduct = new Product({
//       name,
//       category,
//       nutrNum,
//       quantity,
//       description,
//     });

//     // Handle image upload to cloudinary (similar to your existing logic)
//     if (file) {
//       const { secure_url, public_id } = await cloudinary.uploader.upload(
//         file.path
//       );
//       newProduct.image = secure_url;
//       newProduct.imagePublicId = public_id;
//     }

//     await newProduct.save();

//     res.status(201).json({
//       product: {
//         id: newProduct._id,
//         name: newProduct.name,
//         // Add other product details as needed
//       },
//     });
//   } catch (err) {
//     sendError(res, err.message);
//   }
// };

// exports.getProduct = async (req, res) => {
//   try {
//     const productId = req.params.id;

//     if (!isValidObjectId(productId)) {
//       return sendError(res, "Invalid product ID");
//     }

//     const product = await Product.findById(productId);

//     if (!product) {
//       return sendError(res, "Product not found", 404);
//     }

//     res.json({ product });
//   } catch (err) {
//     sendError(res, err.message);
//   }
// };

// exports.updateProduct = async (req, res) => {
//   try {
//     const productId = req.params.id;
//     const { file, body } = req;

//     if (!isValidObjectId(productId)) {
//       return sendError(res, "Invalid product ID");
//     }

//     const product = await Product.findById(productId);

//     if (!product) {
//       return sendError(res, "Product not found", 404);
//     }

//     const { name, category, nutrNum, quantity, description } = body;

//     // Update product fields
//     product.name = name;
//     product.category = category;
//     product.nutrNum = nutrNum;
//     product.quantity = quantity;
//     product.description = description;

//     // Handle image update (similar to your create operation)
//     if (file) {
//       const { secure_url, public_id } = await cloudinary.uploader.upload(
//         file.path
//       );
//       product.image = secure_url;
//       product.imagePublicId = public_id;
//     }

//     await product.save();

//     res.json({ product });
//   } catch (err) {
//     sendError(res, err.message);
//   }
// };

// exports.deleteProduct = async (req, res) => {
//   try {
//     const productId = req.params.id;

//     if (!isValidObjectId(productId)) {
//       return sendError(res, "Invalid product ID");
//     }

//     const product = await Product.findById(productId);

//     if (!product) {
//       return sendError(res, "Product not found", 404);
//     }

//     // Handle image deletion from cloudinary (if needed)
//     if (product.imagePublicId) {
//       // Use cloudinary.uploader.destroy to delete the image
//       await cloudinary.uploader.destroy(product.imagePublicId);
//     }

//     // Delete the product from the database
//     await Product.findByIdAndDelete(productId);

//     res.json({ message: "Product deleted successfully" });
//   } catch (err) {
//     sendError(res, err.message);
//   }
// };

const { isValidObjectId } = require("mongoose");
const Product = require("../models/productModels");
const { sendError, uploadImageToCloud } = require("../utils/helper");
const cloudinary = require("../cloud");

exports.createProduct = async (req, res) => {
  const { name, category, nutrNum, quantity, description } = req.body;
  const { file } = req;

  const newProduct = new Product({
    name,
    category,
    nutrNum,
    quantity,
    description,
  });

  if (file) {
    const { url, public_id } = await uploadImageToCloud(file.path);
    newProduct.image = { url, public_id };
  }

  await newProduct.save();
  res.status(201).json({ product: newProduct });
};

exports.updateProduct = async (req, res) => {
  const { name, category, nutrNum, quantity, description } = req.body;
  const { file } = req;
  const { productId } = req.params;

  if (!isValidObjectId(productId)) return sendError(res, "Invalid request!");

  const product = await Product.findById(productId);
  if (!product) return sendError(res, "Invalid request, product not found!");

  const public_id = product.image?.public_id;

  // Remove old image if there was one
  if (public_id && file) {
    const { result } = await cloudinary.uploader.destroy(public_id);
    if (result !== "ok") {
      return sendError(res, "Could not remove image from cloud!");
    }
  }

  // Upload a new image if there is one
  if (file) {
    const { url, public_id } = await uploadImageToCloud(file.path);
    product.image = { url, public_id };
  }

  product.name = name;
  product.category = category;
  product.nutrNum = nutrNum;
  product.quantity = quantity;
  product.description = description;

  await product.save();

  res.status(201).json({ product });
};

exports.removeProduct = async (req, res) => {
  const { productId } = req.params;

  if (!isValidObjectId(productId)) return sendError(res, "Invalid request!");

  const product = await Product.findById(productId);
  if (!product) return sendError(res, "Invalid request, product not found!");

  const public_id = product.image?.public_id;

  // Remove old image if there was one
  if (public_id) {
    const { result } = await cloudinary.uploader.destroy(public_id);
    if (result !== "ok") {
      return sendError(res, "Could not remove image from cloud!");
    }
  }

  await Product.findByIdAndDelete(productId);

  res.json({ message: "Product removed successfully." });
};

exports.searchProduct = async (req, res) => {
  const { name } = req.query;

  if (!name.trim()) return sendError(res, "Invalid request!");

  const products = await Product.find({
    name: { $regex: name, $options: "i" },
  });

  res.json({ results: products });
};

// exports.getLatestProducts = async (req, res) => {
//   const result = await Product.find().sort({ createdAt: "-1" }).limit(12);
//   res.json(result);
// };

exports.getProduct = async (req, res) => {
  const { id } = req.params;

  if (!isValidObjectId(id)) return sendError(res, "Invalid request!");

  const product = await Product.findById(id);
  if (!product)
    return sendError(res, "Invalid request, product not found!", 404);

  res.json(product);
};

exports.getProducts = async (req, res) => {
  const { pageNo, limit } = req.query;

  const products = await Product.find({})
    .sort({ createdAt: -1 })
    .skip(parseInt(pageNo) * parseInt(limit))
    .limit(parseInt(limit));

  res.json(products);
};
