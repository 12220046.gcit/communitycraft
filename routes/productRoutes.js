const express = require("express");
const {
  createProduct,
  updateProduct,
  removeProduct,
  getProduct,
  getProducts,
} = require("../controllers/productController");
const { uploadImage } = require("../middlewares/multer"); // For handling product images
// const cloudinary = require("../middlewares/cloudinary"); // For Cloudinary integration
const router = express.Router();

// Create a new product
router.post("/create", uploadImage.single("productImage"), createProduct);

// Update a product by ID
router.post(
  "/update/:productId",
  uploadImage.single("productImage"),
  updateProduct
);

// Delete a product by ID
router.delete("/:productId", removeProduct);

// Get a product by ID
router.get("/single/:productId", getProduct);

// Get a list of products
router.get("/products", getProducts);

module.exports = router;
