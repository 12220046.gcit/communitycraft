const mongoose = require("mongoose");

const productSchema = mongoose.Schema(
  {
    name: {
      type: String,
      trim: true,
      required: true,
    },
    category: {
      type: String,
      trim: true,
      required: true,
    },
    nutrNum: {
      type: Number,
      required: true,
    },
    quantity: {
      type: Number,
      required: true,
    },
    description: {
      type: String,
      trim: true,
      required: true,
    },
    image: {
      type: Object,
      url: String,
      public_id: String,
    },
  },
  { timestamps: true }
);

productSchema.index({ name: "text" });

module.exports = mongoose.model("Product", productSchema);
